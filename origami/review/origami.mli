type point = float * float
(** Punkt na p�aszczy�nie *)

type kartka = point -> int
(** Posk�adana kartka: ile razy kartk� przebije szpilka wbita w danym
punkcie *)

val prostokat : point -> point -> kartka
(** [prostokat p1 p2] zwraca kartk�, reprezentuj�c� domkni�ty
prostok�t o bokach r�wnoleg�ych do osi uk�adu wsp�rz�dnych i lewym
dolnym rogu [p1] a prawym g�rnym [p2]. Punkt [p1] musi wi�c by�
nieostro na lewo i w d� od punktu [p2]. Gdy w kartk� t� wbije si� 
szpilk� wewn�trz (lub na kraw�dziach) prostok�ta, kartka zostanie
przebita 1 raz, w pozosta�ych przypadkach 0 razy *)

val kolko : point -> float -> kartka
(** [kolko p r] zwraca kartk�, reprezentuj�c� k�ko domkni�te o �rodku
w punkcie [p] i promieniu [r] *)

val zloz : point -> point -> kartka -> kartka
(** [zloz p1 p2 k] sk�ada kartk� [k] wzd�u� prostej przechodz�cej
przez punkty [p1] i [p2] (musz� to by� r�ne punkty). Papier jest
sk�adany w ten spos�b, �e z prawej strony prostej (patrz�c w kierunku
od [p1] do [p2]) jest przek�adany na lew�. Wynikiem funkcji jest
z�o�ona kartka. Jej przebicie po prawej stronie prostej powinno wi�c
zwr�ci� 0. Przebicie dok�adnie na prostej powinno zwr�ci� tyle samo,
co przebicie kartki przed z�o�eniem. Po stronie lewej - tyle co przed
z�o�eniem plus przebicie roz�o�onej kartki w punkcie, kt�ry na�o�y�
si� na punkt przebicia. *)

val skladaj : (point * point) list -> kartka -> kartka
(** [skladaj [(p1_1,p2_1);...;(p1_n,p2_n)] k = zloz p1_n p2_n (zloz ... (zloz p1_1 p2_1 k)...)] 
czyli wynikiem jest z�o�enie kartki [k] kolejno wzd�u� wszystkich prostych 
z listy *)