(* Autor: Wojciech Ciszewski *)
(* Code review: Rafał Pragacz *)

(* Para (x, y) to punkt o współrzędnych kartezjańskich (x, y) *)

type point = float * float;;

type kartka = point -> int;;

(* Jeśli dwie liczby są bliżej siebie niż epsilon, są uznawane za równe *)

let epsilon = 0.000001;;

let (=~) a b = a >= b -. epsilon && a <= b +. epsilon;;

let (>=~) a b = a > b || a =~ b;;

let (<=~) a b = a < b || a =~ b;;

let kwad x = x *. x;;

(* Mnożenie liczb zespolonych - przydatne przy symetrycznym odbijaniu punktu *)
(* (a, b) jest tu traktowane jak liczba a + b * i *)

let mnozzesp (x1, y1) (x2, y2) = (x1 *. x2 -. y1 *. y2, x1 *. y2 +. x2 *. y1);;

let modul (x, y) = sqrt (kwad x +. kwad y);;

(* Odwrotność liczby zespolonej, stosowana przy dzieleniu *)

let odwzesp (x, y) =
  let m = kwad (modul (x, y)) in (x /. m, -.y /. m);;

(* Iloczyn wektorowy do sprawdzania, po której stronie prostej jest punkt *)

let ilwek (x1, y1) (x2, y2) = x1 *. y2 -. y1 *. x2;;

(* Odbicie korzysta z faktu, że przy mnożeniu liczb zespolonych mnożone są *)
(* ich moduły, a dodawane argumenty. Mnożone są liczby odpowiadające położeniu *)
(* punktów (x2, y2) oraz (x, y) względem punktu (x1, y1) (odbijany jest punkt (x, y)) *)
(* względem prostej przez punkty (x1, y1) (x2, y2) *)

let odbicie (x1, y1) (x2, y2) (x, y) =
  let wekpr = (x2 -. x1, y2 -. y1) and wekpkt = (x -. x1, y -. y1) in
    let (a, b) = mnozzesp (mnozzesp wekpr wekpr) (odwzesp wekpkt)
    and skala = kwad (modul wekpkt) /. kwad (modul wekpr) in
      (x1 +. a *. skala, y1 +. b *. skala);;

(* Odległość punktu od prostej - potrzebna jedynie do sprawdzania, czy punkt należy *)
(* uznać za leżący na prostej. Można by to robić przy pomocy iloczynu skalarnego, ale *)
(* ze względu na przybliżenia wynik zależałby nie tylko od faktycznej odległości *)
(* punktu od prostej, ale też od jego odległości od pierwszego punktu wyznaczającego prostą *)

let odleglosc (x1, y1) (x2, y2) (x, y) =
  (x *. (y2 -. y1) +. y *. (x1 -. x2) +. y1 *. x2 -. x1 *. y2) /.
  modul (y2 -. y1, x1 -. x2);;

let prostokat (x1, y1) (x2, y2) =
  function (x, y) ->
    if x >=~ x1 && x <=~ x2 && y >=~ y1 && y <=~ y2 then 1 else 0;;

let kolko (x1, y1) r =
  function (x, y) ->
    if kwad (x1 -. x) +. kwad (y1 -. y) <=~ kwad r then 1 else 0;;

let zloz (x1, y1) (x2, y2) kar =
  function (x, y) ->
    let wektorowy = ilwek (x2 -. x1, y2 -. y1) (x -. x1, y -. y1)
    and odl = odleglosc (x1, y1) (x2, y2) (x, y)
    in
      if odl =~ 0. then kar (x, y) else
      if wektorowy < 0. then 0 else
        kar (x, y) + kar (odbicie (x1, y1) (x2, y2) (x, y));;

let skladaj lista kar =
  List.fold_left (fun a (p1, p2) -> zloz p1 p2 a) kar lista;;