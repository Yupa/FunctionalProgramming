open Origami;;

let test a b msg = if a=b then print_endline "ok"
  else (print_int a; print_string "<>"; print_int b; print_string " test: "; print_endline msg);;


let p1 = prostokat (0., 0.) (10., 10.)
let k1 = kolko (5., 5.) 5.
let l1 = [((0., 0.), (10., 10.))];;
let l2 = [((8., 0.), (10., 2.));
	  ((6., 0.), (10., 4.));
	  ((4., 0.), (10., 6.));
	  ((2., 0.), (10., 8.));
	  ((0., 0.), (10., 10.));
	  ((0., 2.), (8., 10.));
	  ((0., 4.), (6., 10.));
	  ((0., 6.), (4., 10.));
	  ((0., 8.), (2., 10.))];;

let construct a b n =
  let rec pom (x1, y1) (x2, y2) n a =
    let sx = (x1 +. x2) /. 2. and sy = (y1 +. y2) /. 2. in
    if n=0 then ((sx, sy),a) else
      let (w,wyn) = pom (x1, y1) (sx, sy) (n - 1) a in
	(w,((sx, y1), (sx, y2))::(((sx, sy), (x1, sy))::wyn)) in
  pom a b n [];;
    
let p2 = skladaj l1 p1
let p3 = skladaj l2 p1
let k2 = skladaj l1 k1;;

test (p2 (7., 3.)) 0 "0.1: p2";;
