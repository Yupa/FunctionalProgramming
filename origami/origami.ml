(*Rafal Pragacz, reviewer Wojciech Ciszewski*)
(*testowane na - http://pastebin.com/SWBDRWW0*)

type point = float * float

type kartka = point -> int

(*FUNKCJE POMOCNICZE*)
let eps = 0.000000001
let square x = x *. x;;

(*odleglosc dwoch punktow*)
let odl (a1, b1) (a2, b2) =
  sqrt( square (a1 -. a2) +. square (b1 -. b2) );;

(*dodaje do wektora p2 wektor p1*)
let dodaj (a1, b1) (a2, b2) = ((a2 +. a1), (b2 +. b1));;

(*odejmuje od wektora p2 wektor p1*)
let odejmij (a1, b1) p2 = dodaj (-. a1, -. b1) p2;;

(*wyznacznik macierzy wektorow (p1 - p), (p2 - p) *)
(*okresla po ktorej stronie prostej p1 p2 znajduje sie punkt p w nastepujacy sposob*)
(* >0 - p lezy po lewej stronie prostej, =0 - na prostej, <0 - po prawej*)
let det (a1, b1) (a2, b2) (a, b) =
  (a1 -. a) *. (b2 -. b) -. (a2 -. a) *. (b1 -. b);;

let po_ktorej_stronie p1 p2 p =
  let wyn1 = det p1 p2 (odejmij (eps, eps) p)
  and wyn2 = det p1 p2 (dodaj (eps, eps) p)
  in if wyn1 > 0. && wyn2 > 0. then 1
    else if wyn1 < 0. && wyn2 < 0. then -1
    else 0;;

(*odbija punkt p2 wzgledem prostej 0 - p1*)
let odbij (a1, b1) (a2, b2) =
  ((square a1 *. a2 +. 2.0 *. a1 *. b1 *. b2 -. a2 *. square b1) /. (square a1 +. square b1),
   (square b1 *. b2 +. 2.0 *. b1 *. a1 *. a2 -. b2 *. square a1) /. (square a1 +. square b1));;

(*odbija punkt p wzgledem prostej p1 p2*)
let symetria p1 p2 p =
  dodaj p1 (odbij (odejmij p1 p2) (odejmij p1 p));;

(*FUNKCJE OBLIGATORYJNE*)

let prostokat (a1, b1) (a2, b2) (a, b) =
  if a < a1 -. eps || a > a2 +. eps || b < b1 -. eps || b > b2 +. eps then 0 else 1;;

let kolko p1 r p =
  if odl p1 p > r +. eps then 0 else 1;;

let zloz p1 p2 k p =
  let wyn = po_ktorej_stronie p1 p2 p
  in if wyn = 0 then (k p)
    else if wyn > 0 then (k p) + (k (symetria p1 p2 p))
    else 0;;

let skladaj l k =
  List.fold_left (fun a (p1, p2) -> zloz p1 p2 a) k l;;
