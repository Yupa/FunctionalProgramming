(*
 * Autor: Bartłomiej Karasek
 * Recenzent: Rafał Pragacz
 *)

(* Node(lewe poddrzewo, lewy koniec przedzialu, prawy koniec przedzialu, prawe poddrzewo, wysokosc, ilosc liczb) *)
type t =
  | Empty
  | Node of t * int * int * t * int * int;;

(* zwraca wysokosc drzewa *)
let height = function
  | Node (_, _, _, _, h, _) -> h
  | Empty -> 0;;

(* zwraca ilosc liczb przechowywanych w (pod)drzewie *)
let size = function
  | Node (_, _, _, _, _, n) -> n
  | Empty -> 0;;

let empty = Empty;;

let is_empty x = x = Empty;;


let make l kx ky r =
  Node (l, kx, ky, r, max (height l) (height r) + 1, size l + size r + ky - kx + 1);;

let rec bal l kx ky r =
  let hl = height l in
  let hr = height r in
  if hl > hr + 2 then
    match l with
    | Node (ll, lkx, lky, lr, _, _) ->
        if height ll >= height lr then bal ll lkx lky (bal lr kx ky r)
        else
          (match lr with
          | Node (lrl, lrkx, lrky, lrr, _, _) ->
              bal (bal ll lkx lky lrl) lrkx lrky (bal lrr kx ky r)
          | Empty -> assert false)
    | Empty -> assert false
  else if hr > hl + 2 then
    match r with
    | Node (rl, rkx, rky, rr, _, _) ->
        if height rr >= height rl then bal (bal l kx ky rl) rkx rky rr
        else
          (match rl with
          | Node (rll, rlkx, rlky, rlr, _, _) ->
              bal (bal l kx ky rll) rlkx rlky (bal rlr rkx rky rr)
          | Empty -> assert false)
    | Empty -> assert false
  else 
    Node (l, kx, ky, r, max hl hr + 1, size l + size r + ky - kx + 1);;


(* zwraca drzewo z elementami mniejszymi niz x *)
let rec getLeft x = function
    | Node (l, lft, rht, r, _, _) -> 
      if x <= lft then getLeft x l
      else if x <= rht then bal (getLeft x l) lft (x - 1) Empty
      else bal l lft rht (getLeft x r)
    | Empty -> Empty;;

let rec getRight x = function
    | Node (l, lft, rht, r, _, _) -> 
      if x >= rht then getRight x r
      else if x >= lft then bal Empty (x + 1) rht (getRight x r)
      else bal (getRight x l) lft rht r
    | Empty -> Empty;;

(* sprawdza czy x jest w drzewie *)
let rec mem x = function
  | Node (l, lft, rht, r, _, _) ->
    if x < lft then (mem x l)
    else if x <= rht then true
    else (mem x r)
  | Empty -> false;;


let split x set = getLeft x set, mem x set, getRight x set;;

(* znajduje najdalsza liczbe na lewa znajdujaca sie w tym samym przedziale co x *)
let rec leftmost x = function
  | Node (l, lft, rht, r, _, _) ->
    if x < lft then leftmost x l
    else if x <= rht then lft
    else leftmost x r
  | Empty -> (-max_int);;

let rec rightmost x = function
  | Node (l, lft, rht, r, _, _) ->
    if x > rht then rightmost x r
    else if x >= lft then rht
    else rightmost x l
  | Empty -> (-max_int);;

(* rozszerza przedzial v o liczby znajdujace sie w drzewie,
   nastepnie tworzy drzewo na podstawie tego przedzialu 
   z wykorzystaniem getLeft i getRight *)
let add v set =
  let left = 
    if fst v <> -min_int && mem (fst v - 1) set then
      leftmost (fst v - 1) set
    else
      fst v in
  let right = 
    if snd v <> max_int && mem (snd v + 1) set then
      rightmost (snd v + 1) set
    else
      snd v in
  bal (getLeft left set) left right (getRight right set);;

let rec min_elt = function
  | Node (Empty, lft, rht, _, _, _) -> lft, rht
  | Node (l, _, _, _, _, _) -> min_elt l
  | Empty -> raise Not_found;;

let rec remove_min_elt = function
  | Node (Empty, _, _, r, _, _) -> r
  | Node (l, lft, rht, r, _, _) -> bal (remove_min_elt l) lft rht r
  | Empty -> invalid_arg "PSet.remove_min_elt";;

let merge t1 t2 =
  match t1, t2 with
  | Empty, _ -> t2
  | _, Empty -> t1
  | _ ->
      let lft, rht = min_elt t2 in
      bal t1 lft rht (remove_min_elt t2);;

let remove v set = merge (getLeft (fst v) set) (getRight (snd v) set);;


let iter f set =
  let rec loop = function
    | Empty -> ()
    | Node (l, lft, rht, r, _, _) -> loop l; f (lft, rht); loop r in
  loop set;;

let fold f set acc =
  let rec loop acc = function
    | Empty -> acc
    | Node (l, lft, rht, r, _, _) ->
          loop (f (lft, rht) (loop acc l)) r in
  loop acc set;;

let elements set = 
  let rec loop acc = function
      Empty -> acc
    | Node (l, lft, rht, r, _, _) -> loop ((lft, rht) :: loop acc r) l in
  loop [] set;;

(* wykorzystuje przeskakiwanie int'ow, i dla ujemnych liczb wypisuje max_int *)
let below x set = 
  let result = (size (getLeft (x) set)) in
  if result < 0 then max_int else result + if (mem x set) then 1 else 0;;

let x = add (max_int, max_int) empty;;
let x = add (-min_int, -min_int) x;;
