open ISet;;

let rec dodaj k n =
  if (n = 0) then k
  else
    let m = Random.int 10000000
    and l = Random.int 10000000
    in dodaj (add (m, l) k) (n - 1);;

let rec zdejmij k n =
  if (n = 0) then ()
  else
    let m = Random.int 10000000
    and l = Random.int 10000000
    in zdejmij (remove (m, l) k) (n - 1);;

let testuj n =
  zdejmij (dodaj empty n) n;;

testuj 100000000;;
