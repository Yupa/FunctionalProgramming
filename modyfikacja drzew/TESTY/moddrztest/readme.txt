Sprawdzaczka składa się z 2 modułów:
 - Gen generuje listę testów (na wejściu wprowadza się jej długość i zakres wartości).
   Testowane są operacje add, remove, mem, split, elements oraz below. Inne operacje nie są sprawdzanr
   (ale jak ktoś je napisze to bardzo chętnie zaktualizuję sprawdzarkę).
   Operacje są generowane z różną częstotliwością zależną od odpowiednich stałych
   w pliku gen.ml.
 - Brut to rozwiązanie brutalne, implementujące na zawsze posortowanych listach
   operacje add, remove, mem, split, elements oraz below.
Oraz pliku test.ml który wczytuje parametry
(długość, minimalna oraz maksymalna wartość na drzewie - podawać w osobnych liniach bo inaczej wywala
błąd wczytywania)
Oraz w nieskończoność generuje test i sprawdza czy wyniki zgadzają się z wynikami bruta.
Jeśli tak to wypisuje numer tego testu, a jeśli gdzieś był błąd to wypisuje test do tego miejsca
i kończy działanie.


Aby użyć wystarczy użyć poleceń (w katalogu z rozpakowanym archiwum oraz plikami iSet.ml i iSet.mli):
ocamlbuild test.native
./test.native
