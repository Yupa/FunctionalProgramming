(* Generator testów do przedziałowych drzew avl.
   Funkcja get_test otrzymuję ilość testów Add, Remove i Mem w bloku
   i zwraca listę wartości typu test opisującą blok testów.
   Na początku bloku są testy Add i Remove, oraz Split w którym bool określa czy brać
   lewe czy prawe poddrzewo do dalszych testów.
   Na końcu bloku będzie test Elements i test Below.
 *)


type test =
  | Add of (int * int)
  | Remove of (int * int)
  | Mem of int
  | Elements
  | Below of int
  | Split of (int * bool)

val gen_test : int -> int * int -> test list

