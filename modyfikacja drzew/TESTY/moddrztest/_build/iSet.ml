(*Rafal Pragacz, review Bartlomiej Karasek*)
(*testowane na - http://pastebin.com/GDj49P13*)

(*TYP = Lewe poddrzewo, wartosc w wezle, prawe poddrzewo, wysokosc drzewa, ilosc elementow*)
type t =
  | Node of t * (int * int) * t * int * int
  | Empty;;

let empty = Empty;;

let is_empty set = (set = Empty);;

(*porownuje czy przedzialy sie polacza*)
let cmp (a, b) (c, d) =
  if b < (c -1) then -1
  else if d < (a - 1) then 1
  else 0;;

(*porownuje czy przedzialy maja czesc wspolna*)
let cmp2 (a, b) (c, d) =
  if b < c then -1
  else if d < a then 1
  else 0;;

(*FUNKCJE POMOCNICZE*)
let height set =
  match set with
  | Empty -> 0
  | Node (_, _, _, h, _) -> h;;

let count set =
  match set with
  | Empty -> 0
  | Node (_, _, _, _, n) -> n;;

let make l v r =
  Node (l, v, r, max (height l) (height r) + 1, count(l) + count(r) + (snd v) - (fst v) + 1);;

(*balansuje drzewo zeby bylo zrownowazone*)
let rec bal l v r =
  let hl = height l in
  let hr = height r in
  if hl > hr + 2 then
    match l with
    | Node (ll, lv, lr, _, _) ->
        if height ll >= height lr then bal ll lv (bal lr v r)
        else
          (match lr with
          | Node (lrl, lrv, lrr, _, _) ->
              bal (bal ll lv lrl) lrv (bal lrr v r)
          | Empty -> assert false)
    | Empty -> assert false
  else if hr > hl + 2 then
    match r with
    | Node (rl, rv, rr, _, _) ->
        if height rr >= height rl then bal (bal l v rl) rv rr
        else
          (match rl with
          | Node (rll, rlv, rlr, _, _) ->
              bal (bal l v rll) rlv (bal rlr rv rr)
          | Empty -> assert false)
    | Empty -> assert false
  else make l v r;;

(*Funkcja znajdujaca poczatek przedzialu, ktory powstanie z dolaczenia przedzialu x*)
let rec find_min x set =
  match set with
  | Node (l, v, r, h, n) ->
    let c = cmp x v in
      if c < 0 then find_min x l
      else if c > 0 then find_min x r
      else if (fst x < fst v) then find_min x l
      else (fst v)
  | Empty -> (fst x);;

(*Funkcja znajdujaca koniec przedzialu, ktory powstanie z dolaczenia przedzialu x*)
let rec find_max x set =
  match set with
  | Node (l, v, r, h, n) ->
    let c = cmp x v in
      if c < 0 then find_max x l
      else if c > 0 then find_max x r
      else if (snd x > snd v) then find_max x r
      else (snd v)
  | Empty -> (snd x);;

(*Usuwa z drzewa wszystkie elementy wieksze rowne x*)
let rec remove_larger x set =
  match set with
  | Node (l, v, r, h, n) ->
    if x <= (fst v) then remove_larger x l
    else if x > (snd v) then bal l v (remove_larger x r)
    else bal l (fst v, x - 1) Empty
  | Empty -> Empty;;

(*Usuwa z drzewa wszystkie elementy mniejsze rowne x*)
let rec remove_smaller x set =
  match set with
  | Node (l, v, r, h, n) ->
    if x >= (snd v) then remove_smaller x r
    else if x < (fst v) then bal (remove_smaller x l) v r
    else bal Empty (x + 1, snd v) r
  | Empty -> Empty;;

(*znajduje minimalny element drzewa*)
let rec min_el = function
  | Node (Empty, k, _, _, _) -> k
  | Node (l, _, _, _, _) -> min_el l
  | Empty -> raise Not_found;;

(*laczy dwa drzewa*)
let merge set1 set2 =
  match (set1, set2) with
  | Empty, _ -> set2
  | _, Empty -> set1
  | _, _ -> let v = min_el set2 in
    bal set1 v (remove_smaller (snd v) set2);;

(*dodaje przedzial nie majacy czesci wspolnej z drzewem*)
let rec add_simple x set =
   match set with
  | Node (l, v, r, h, n) ->
    let c = cmp x v in
      if c < 0 then bal (add_simple x l) v r
      else bal l v (add_simple x r)
  | Empty -> make Empty x Empty;;

(*KONIEC FUNKCJI POMOCNICZYCH*)

let remove x set =
  let lewe = remove_larger (fst x) (set) in
  let prawe = remove_smaller (snd x) (set) in
  merge (lewe) (prawe);;

(*znajduje przedzial jaki powstanie z dodawanego przedzialu, a nastepnie go usuwam i wstawiam*)
let rec add x set =
  let d = find_min x set in
  let g = find_max x set in
  add_simple (d, g) (remove (d, g) set);;

let mem x set =
  let rec loop = function
    | Node (l, k, r, _, _) ->
      let c = cmp2 (x, x) k in
        if c = 0 then true
        else loop (if c < 0 then l else r)
    | Empty -> false
  in loop set;;

let iter f set =
  let rec loop = function
    | Empty -> ()
    | Node (l, k, r, _, _) -> loop l; f k; loop r
  in loop set;;

let fold f set acc =
  let rec loop acc = function
    | Empty -> acc
    | Node (l, k, r, _, _) -> loop (f k (loop acc l)) r
  in loop acc set;;

let elements set =
  let rec loop acc = function
    | Empty -> acc
    | Node(l, k, r, _, _) -> loop (k :: loop acc r) l
  in loop [] set;;

(*jesli elementow jest ponad max_int to po zliczeniu wynik wyjdzie <= 0*)
let below x set =
  let wyn = remove_larger (x+1) set in
    if is_empty wyn then 0
    else if count wyn <= 0 then max_int
    else count wyn;;

let split x set = (remove_larger x set, mem x set, remove_smaller x set);;
