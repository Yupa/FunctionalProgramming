(* Generator testów do przedziałowych drzew avl.
   Funkcja get_test otrzymuję ilość testów Add, Remove i Mem w bloku
   i zwraca listę wartości typu test opisującą blok testów.
   Na początku bloku są testy Add i Remove, oraz Split w którym bool określa czy brać
   lewe czy prawe poddrzewo do dalszych testów.
   Na końcu bloku będzie test Elements i test Below.
 *)


type test =
  | Add of (int * int)
  | Remove of (int * int)
  | Mem of int
  | Elements
  | Below of int
  | Split of (int * bool)

(* Stałe określające częstotliwość występowania odpowiednich testów.
   Im większa różnica między kolejnymi stałymi, tym częściej test będzie występował *)
let add = 7 and remove = 12 and mem = 25 and split = 27 and below = 32;;

let gen_test n (mn, mx) =
  Random.self_init ();
  let rec pom n a =
    if n = 0 then a else
    let t = Random.int below
    and p = mn + Random.int (mx - mn) in
    let k = p + Random.int (mx - p) in
    let a =
      if t < add then (Add (p, k))::a else
      if t < remove then (Remove (p, k))::a else
      if t < mem then (Mem p)::a else
      if t < split then (Split (p, (Random.bool ()) ))::a else
      (Below p)::a
    in pom (n - 1) a
  in
  List.rev ((Elements)::(pom n []));;

