open Int64

type bset = (int * int) list
  
let bempty = []

let cmp a b = if fst a > (snd b) + 1 then 1 else if snd a < (fst b) - 1 then -1 else 0

let cmp_int a b = if a > snd b then 1 else if a < fst b then -1 else 0
  
let rec badd ((p, k) as x) = function
  | [] -> [(p, k)]
  | ((ap, ak) as h)::t ->
      let c = cmp h x in
      if c = 0 then badd (min p ap, max k ak) t else
      if c > 0 then x::(h::t) else h::(badd x t)

let rec bremove ((p, k) as x) = function
  | [] -> []
  | ((ap, ak) as h)::t ->
      let c = cmp h x in
      if c = 0 then
	let us = (ak + 1, k)
	and lp = (ap, p - 1)
	and pp = (k + 1, ak) in
	let t = if fst us <= snd us then bremove us t else t in
	let w = if fst pp <= snd pp then pp::t else t in
	let w = if fst lp <= snd lp then lp::w else w in w
      else if c > 0 then h::t else h::(bremove x t)

let rec bmem x = function
  | [] -> false
  | h::t ->
      let c = cmp_int x h in
      if c = 0 then true else
      if c < 0 then false else bmem x t

let bbelow x l =
  let rec pom a = function
    | [] -> a
    | (p, k)::t -> if p > x then a else
      if k < x then
	let w = succ (add (of_int a) (add (of_int k) (neg (of_int p)))) in
	if w < of_int Pervasives.max_int && w >= zero then pom (to_int w) t else Pervasives.max_int
      else
	let w = succ (add (of_int a) (add (of_int x) (neg (of_int p)))) in
	if w < of_int Pervasives.max_int && w >= zero then to_int w else Pervasives.max_int
  in
  pom 0 l

let bsplit x l =
  let rec pom a = function
    | [] -> (List.rev a, false, [])
    | ((p, k)::t) as l ->
	if x < p then (List.rev a, false, l) else
	if x > k then pom ((p, k)::a) t else
	let ll = (p, x - 1)
	and pp = (x + 1, k) in
	let a = if fst ll <= snd ll then ll::a else a in
	let t = if fst pp <= snd pp then pp::t else t in
	(List.rev a, true, t) in
  pom [] l
    
let belements x = x;;
