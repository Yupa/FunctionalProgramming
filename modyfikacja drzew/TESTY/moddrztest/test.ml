open Gen
open Brut
open ISet

let rec wypisz n = function
  | [] -> ()
  | h::t -> (match h with
    | Add (p, k) -> print_endline ("Add: "^(string_of_int p)^" "^(string_of_int k))
    | Remove (p, k) -> print_endline ("Remove: "^(string_of_int p)^" "^(string_of_int k))
    | Mem x -> print_endline ("Mem: "^(string_of_int x))
    | Split (x, b) -> print_endline ("Split: "^(string_of_int x)^" "^(string_of_bool b))
    | Elements -> print_endline "Elements"
    | Below x-> print_endline ("Below: "^(string_of_int x)));
	if n > 0 then wypisz (n - 1) t;;
    
  
let testoj n range =
  let a = empty
  and b = bempty in
  let ltest = gen_test n range in
  let rec pom a b = function
    | [] -> -1
    | h::t -> match h with
      | Add x -> pom (add x a) (badd x b) t
      | Remove x -> pom (remove x a) (bremove x b) t
      | Mem x -> if mem x a = bmem x b then pom a b t else List.length t
      | Split (x, bo) ->
	  let (pa, ba, ka) = split x a
	  and (pb, bb, kb) = bsplit x b in
	  if bb <> ba then List.length t else
	  if bo then pom ka kb t else pom pa pb t
      | Elements -> if elements a = belements b then pom a b t else List.length t
      | Below x -> if below x a = bbelow x b then pom a b t else List.length t
  in
  let w = List.length ltest - pom a b ltest in
  if w <= List.length ltest then ( wypisz (w + 1) ltest; false ) else true

let rec posctesty dltest range n =
  if dltest >=0 0 && testoj dltest range then
    begin
      print_endline (string_of_int n);
      posctesty (dltest - 1) range (n + 1)
    end;;

let dltest = 10000;;
let p = -536870911;;
let k = 536870911;;

posctesty dltest (p, k) 0;;

