type bset

val bempty : bset
      
val badd : int * int -> bset -> bset

val bremove : int * int -> bset -> bset

val bmem : int -> bset -> bool

val bbelow : int -> bset -> int

val bsplit : int -> bset -> bset * bool * bset

val belements : bset -> (int * int) list
