open ISet;;

let rec porownaj_listy l1 l2 =
  match (l1, l2) with
  | ([], []) -> true
  | (hl::tl, hr::tr) ->
    if fst hl != fst hr || snd hl != snd hr then false
    else porownaj_listy tl tr;;

let e = empty;;
let a1 = add (1, 3) e;;
let a2 = add (3, 3) a1;;
let a3 = add (5, 5) a2;;
let a4 = add (7, 7) a3;;
let a5 = add (9, 11) a4;;
let a6 = add (4, 6) a5;;
let (l, czy, r) = split 6 a6;;

assert (porownaj_listy (elements a6) [(1, 7); (9, 11)]);
assert ((below 10 a6) = 8);
assert (porownaj_listy (elements l) [(1,5)]);
assert (czy = true);
assert (porownaj_listy (elements r) [(7, 7); (9, 11)]);
