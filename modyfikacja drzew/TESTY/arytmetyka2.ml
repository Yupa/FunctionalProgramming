open ISet;;

let a = empty;;
assert (is_empty a);;
assert (below 6 a = 0);;
let a = add (3, 5) a;;
assert (elements a =  [(3, 5)]);;
let a = add (7, 9) a;;
assert (below 7 a = 4);;
let a = add (6, 6) a;;
assert (elements a = [(3, 9)]);;
let a = remove (6, 6) a;;
assert (not (mem 6 a));;
assert (mem 8 a);;
assert (not (mem min_int a));;
let b = remove (-32, -16) a;;
assert (not (a == b));; (* prosty test na funkcyjność *)
let a = add (-32, -16) b;;
assert (mem (-30) a);;
assert (elements a = [(-32, -16); (3, 5); (7, 9)]);;
let (_, x, _) = split 0 a;;
assert (not x);;
let a = add (min_int, max_int) a;;
let (_, x, _) = split 0 a;;
assert x;;
let (_, x, _) = split (-42) a;;
assert x;;
assert (elements a = [(min_int, max_int)]);;
assert (below 700 a = max_int);;
assert (not (is_empty a));;
let a = remove (min_int, 0) a;;
assert (below 700 a = 700);;
assert (below (min_int + 5) a = 0);;
let a = remove (10, max_int) a;;
assert (below max_int a = 9);;
assert (not (mem 0 a));;
let a = remove (2, 2) a;;
assert (not (is_empty a));;
assert (not (mem 2 a));;
assert (below 3 a = 2);;
let a = remove (4, 4) a;;
let (_, x, _) = split 2 a;;
assert (not x);;
let a = remove (6, 6) a;;
let a = remove (8, 9) a;;
assert (not (mem 8 a));;
assert (elements a = [(1, 1); (3, 3); (5, 5); (7, 7)]);;
let a = add (9, 9) a;;
assert (elements a = [(1, 1); (3, 3); (5, 5); (7, 7); (9, 9)]);;
assert (fold (fun (x, y) acc -> x * y + acc) a 42 = 207);;
