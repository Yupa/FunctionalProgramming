(*Rafal Pragacz, reviewer Karol Smolak, testy wlasne - http://pastebin.com/qSAskqFY, inne testy - http://pastebin.com/JSUsagKQ*)

(*bool okresla czy wartosc jest przedzialem (true) 
  czy jego dopelnieniem (false)*)
type wartosc = float*float*bool;;

(*FUNKCJE POMOCNICZE*)

let neginf = neg_infinity;;
let inf = infinity;;

(*sprawdzanie czy liczba jest nan*)
let is_nan x = compare x nan = 0;;

(*minimum z 4 liczb*)
let mini a b c d =
  let rec min_bez_nan x y =
    if is_nan x then y
    else if is_nan y then x
    else (min x y)
  in (min_bez_nan (min_bez_nan a b) (min_bez_nan c d));;

(*maksimum z 4 liczb*)
let maks a b c d =
  let rec max_bez_nan x y =
    if is_nan x then y
    else if is_nan y then x
    else (max x y)
  in (max_bez_nan (max_bez_nan a b) (max_bez_nan c d));;

(*Procedura, ktora bierze dwa zbiory wartosci i zwraca ich sume*) 
let rec zlacz_przedzialy (a,b,w1) (c,d,w2) =
  if is_nan a || is_nan b then (c,d,w2)
  else if is_nan c || is_nan d then (a,b,w1)
  else match (w1, w2) with
    |(true, true) ->
      (*przedzialy nachodza na siebie*)
      if b >= c && d >= a then (min a c, max b d, true)
      (*jesli przedzialy nie nachodza to musza to byc (neg_inf,x) i (y,inf)*)
      else (min b d, max a c, false)
    |(true, false) -> 
      if a <= c then (*(a,b) pokrywa sie z (-inf,c)*)
        if b >= d
          then (neginf, inf, true)
        else
          (max b c, d, false)
      else (*(a,b) pokrywa sie z (d, inf)*)
        (c, min a d, false)
    |(false,true) -> zlacz_przedzialy (c,d,w2) (a,b,w1)
    |(false,false) -> 
      if a >= d || c >= b then (neginf, inf, true)
      else (max a c, min b d, false);;

(*KONSTRUKTORY*)
let wartosc_dokladnosc x p = 
  if x >= 0. then
    (x*.((100.-.p)/.100.), x*.((100.+.p)/.100.), true)
  else
    (x*.((100.+.p)/.100.), x*.((100.-.p)/.100.), true);;

let wartosc_od_do x y = (x,y,true);;

let wartosc_dokladna x = (x,x,true);;


(*SELEKTORY*)
let in_wartosc (a,b,w) x =
  if w then x >= a && x <= b
  else x <= a || x >= b;;

let min_wartosc (a,b,w) =
  if w then a
  else neginf;;

let max_wartosc (a,b,w) =
  if w then b
  else inf;;

let sr_wartosc w =
  (min_wartosc w +. max_wartosc w)/. 2.;;


(*MODYFIKATORY*)
let rec plus (a,b,w1) (c,d,w2) =
  match (w1,w2) with
  |(true,true) -> (a+.c, b+.d, true)
  |(true,false) -> (*dopelnienie zbioru (c,d) to zbiory (-inf,c) U (d,inf)*)
    zlacz_przedzialy
    (plus (a,b,w1) (neginf, c, true))
    (plus (a,b,w1) (d, inf, true))
  |(false,true)-> plus (c,d,w2) (a,b,w1)
  |(false,false) -> (neginf, inf, true);;

(* odjecie przedzialu to dodanie po pomnozeniu przez -1 *)
let minus (a,b,w1) (c,d,w2) =
  plus (a,b,w1) (-.d, -.c, w2);;

let rec razy (a,b,w1) (c,d,w2) =
  if is_nan c || is_nan d || is_nan a || is_nan b then
    (nan, nan, true)
  (*mnozenie przez (0,0) da (0,0)*)
  else if (a = 0. && b = 0.) || (c = 0. && d = 0.) then (0.,0.,true)
  else match (w1,w2) with
    |(true,true) ->
      ((mini (a*.c) (a*.d) (b*.c) (b*.d)) ,
      (maks (a*.c) (a*.d) (b*.c) (b*.d)) , true)
    |(true,false) ->
      zlacz_przedzialy
      (razy (a,b,w1) (neginf, c, true))
      (razy (a,b,w1) (d, inf, true))
    |(false,true) -> razy (c,d,w2) (a,b,w1)
    |(false,false) ->
      zlacz_przedzialy
      (razy (neginf, a, true) (c,d,w2))
      (razy (b,inf,true) (c,d,w2));;

(*dzielenie to mnozenie przez odwrotnosc*)
let rec podzielic (a,b,w1) (c,d,w2) =
  (*nie mozna dzielic przez zero!*)
  if c = 0. && d = 0. then (nan, nan, true)
  else match (w1,w2) with
    |(true,true) ->
      if (c = neginf && d = inf) then 
        razy (a,b,w1) (neginf, inf, true)
      else if c = 0. then (*dzielenie przez (0, x)*)
        razy (a,b,w1) (1./.d,inf,true)
      else if d = 0. then (*dzielenie przez (-x, 0)*)
        razy (a,b,w1) (neginf, 1./.c, true)
      else if (c*.d > 0.) then (*przedzial nie zawiera 0*)
        razy (a,b,w1) (1./.d,1./.c,true)
      else 
        razy (a,b,w1) (1./.c,1./.d,false)
    |(true,false) ->
      zlacz_przedzialy
      (podzielic (a,b,w1) (neginf,c,true))
      (podzielic (a,b,w1) (d,inf,true))
    |(false,true) ->
      zlacz_przedzialy
      (podzielic (neginf,a,true) (c,d,w2))
      (podzielic (b,inf,true) (c,d,w2))
    |(false,false) -> (neginf, inf, true);;
