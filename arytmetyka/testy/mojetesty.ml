open Arytmetyka

let jed = wartosc_dokladna 1.
let zero = wartosc_dokladna 0.
 
let a = zero;;
assert((sr_wartosc a, max_wartosc a, min_wartosc a) = (0.,0.,0.));;
 
let a = wartosc_od_do 0. 1. (*(0,1)*)
let b = podzielic a a (*0, inf*)
let c = razy b zero;; (*0,0*)
assert((sr_wartosc c, max_wartosc c, min_wartosc c) = (0.,0.,0.));;
 
let a = podzielic jed zero;; (*nan*)
assert(compare (min_wartosc a) nan = 0);
assert(compare (max_wartosc a) nan = 0);
assert(compare (sr_wartosc a) nan = 0);;
 
let a = wartosc_dokladnosc 1. 110.;; (*-0.1, 2.1*)
assert(in_wartosc a (-.0.1));
assert(in_wartosc a (2.1));;
 
let a = wartosc_od_do (-.3.) 0.
let b = wartosc_od_do 0. 1.
let c = podzielic a b;; (*-inf, 0*)
assert(max_wartosc c = 0.);
assert(min_wartosc c = neg_infinity);
assert(sr_wartosc c = neg_infinity);;
 
let a = wartosc_od_do 1. 4.
let b = wartosc_od_do (-.2.) 3.
let c = podzielic a b (*(-inf, -1/2) U (1/3, inf) *)
let d = podzielic c b (*(-inf, -1/6) U (1/9, inf) *)
let e = plus d jed (*(-inf, 5/6) U (10/9, inf)*)
let f = sr_wartosc (podzielic jed (wartosc_dokladna 9.));;(*1/9*)
assert(compare (sr_wartosc d) nan = 0);
assert(in_wartosc d 0.12);
assert(in_wartosc d 0. = false);
assert(in_wartosc d (-0.125) = false);
assert(in_wartosc d f = true);
assert(in_wartosc e 1. = false);;
