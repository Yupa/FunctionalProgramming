(* Wiktoria Kośny *)
(* code review: Rafał Pragacz *)
(* testy z grupy Informatyka 2016 na facebooku *)

open Array;;

exception Solution of int

let n = ref 0
let qact = ref [] 
let qnext = ref []
let vis = Hashtbl.create 10000 

let rec gcd a b =
  if (b < a) then gcd b a else
  if (a = 0) then b else
  gcd (b mod a) a
  
let add_state res w finish =
  if(not (Hashtbl.mem vis w)) then (
    if (w = finish) then raise (Solution (res + 1));
    Hashtbl.add vis w (res + 1);
    qnext := w :: !qnext 
  )
   
let move finish cup v =
  let v_res = Hashtbl.find vis v in 
  
  for i = 0 to !n - 1 do
    (* fill a cup *) 
    if (v.(i) <> cup.(i)) then (
      let w = copy v in
      w.(i) <- cup.(i);
      add_state v_res w finish;
    );
      
    (* pour out *)  
    if (v.(i) <> 0) then (
      let w = copy v in
      w.(i) <- 0;
      add_state v_res w finish;
      );
      
    (* transfer between cups*)
    for j = 0 to !n - 1 do
      if (i <> j && v.(i) <> 0 && v.(j) <> cup.(j)) then (
        let change = min v.(i) (cup.(j) - v.(j)) in
        let w = copy v in
        w.(i) <- v.(i) - change;
        w.(j) <- v.(j) + change;
        add_state v_res w finish;
      )
    done       
  done
  
let przelewanka tab =
  n := length tab;
  let cup = map fst tab in
  let finish = map snd tab in
  Hashtbl.clear vis;
  qact := [];
  qnext := [];
  
  (* check obvious conditions making configuration impossible *)
  let cup_gcd = fold_left gcd 0 cup in
  if (cup_gcd = 0) then 0 else 
  let f a x = a || (x mod cup_gcd <> 0) in
  let non_div = fold_left f false finish in
  if (non_div) then -1 else
  
  let g a (c, x) = a || (x == 0 || x == c) in
  let is_empty_or_full = fold_left g false tab in
  if (not is_empty_or_full) then -1 else
  
  (* BFS on possible states *)
  try 
    add_state (-1) (make !n 0) finish;
    qact := !qnext;
    qnext := [];  
    while !qact <> [] do
      List.iter (move finish cup) !qact;
      qact := !qnext;
      qnext := [];    
    done;
    -1
  with 
    Solution x -> x 
