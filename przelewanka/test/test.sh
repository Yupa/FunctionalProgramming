#!/bin/bash

for plik in in/*.in
do
    nazwa=$(basename $plik .in)
    ./gen_out.native < $plik > temp
    d=$(diff temp out/$nazwa.out)
    if [ "$d" != "" ] 
    then 
	echo "$nazwa\n" 
    fi
done
