open Przelewanka;;

assert(przelewanka [|(5, 0); (3, 0); (1, 0) |] = 0);;
assert(przelewanka [|(5, 5); (3, 3); (1, 1) |] = 3);;
assert(przelewanka [|(5, 3); (3, 3); (1, 0) |] = 3);;
assert(przelewanka [|(5, 2); (3, 2); (1, 0) |] = 4);;
assert(przelewanka [|(5, 1); (3, 1); (7, 1) |] = -1);;


assert(przelewanka [|(5, 1); (3, 1); (7, 0) |] = 7);;

assert(przelewanka [|(5,1); (3, 1); (7, 7) |] = 7);;
assert(przelewanka [|(5,0); (3, 1); (7, 1) |] = 6);;
assert(przelewanka [|(5,1); (3, 0); (7, 1) |] = 7);;
assert(przelewanka [|(5,5); (3, 1); (7, 1) |] = 5);;
assert(przelewanka [|(5,1); (3, 0); (7, 1) |] = 7);;
assert(przelewanka [|(5,3); (3, 3); (7, 1) |] = 4);;
assert(przelewanka [|(5,3); (3, 0); (7, 4) |] = 3);;
assert(przelewanka [|(5,0); (3, 3); (7, 4) |] = 2);;
assert(przelewanka [|(5,0); (3, 0); (7, 7) |] = 1);;

print_endline "ok";;