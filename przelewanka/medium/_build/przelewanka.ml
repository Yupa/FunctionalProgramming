(* Rafal Pragacz, *)
(* review Wiktoria Kosny *)

exception Wynik of int;;

let rec gcd x y =
	if x = 0 then y
	else if y = 0 then x
	else if y > x then gcd x (y mod x)
	else if x > y then gcd (x mod y) y
	else x;;

let gcd_of_glasses t = Array.fold_left gcd t.(0) t;;

let przelewanka l =
  let n = Array.length l in
  if n = 0 then 0 else
  let my_hash = Hashtbl.create (n*n*100) in
  let s = Array.init n (fun i -> (snd l.(i))) in
  let s2 = Array.init n (fun i -> (fst l.(i))) in
  let gcd = gcd_of_glasses s in
  let gcd2 = gcd_of_glasses s2 in
  if gcd = 0 || gcd2 = 0 then 0
  else if not (gcd mod gcd2 = 0) then -1
  else
	  let q = Queue.create () in
	  Queue.add ((Array.make n 0), 0) q;
	  try
	  begin
		  while not (Queue.is_empty q) do
			let (t, moves) = Queue.pop q in
			if t = s then raise (Wynik moves);
			for i = 0 to (n - 1) do
				for j = 0 to (n - 1) do
				  if i = j then
					let t1 = Array.copy t in
					let t2 = Array.copy t in
					begin
						t1.(i) <- fst l.(i);
						if not (Hashtbl.mem my_hash t1) then
						begin
					  		Queue.add (t1, moves + 1) q;
					  		Hashtbl.add my_hash t1 (moves + 1);
					  	end;
						t2.(i) <- 0;
						if not (Hashtbl.mem my_hash t2) then
						begin
					  		Queue.add (t2, moves + 1) q;
					  		Hashtbl.add my_hash t2 (moves + 1);
					  	end;
				  	end
				  else
					begin
					  let t3 = Array.copy t in
					  t3.(j) <- min (fst l.(j)) (t.(i) + t.(j));
					  t3.(i) <- max (t.(i) + t.(j) - fst l.(j)) 0;
					  if not (Hashtbl.mem my_hash t3) then
					  begin
						Queue.add (t3, moves + 1) q;
					  	Hashtbl.add my_hash t3 (moves + 1);
					  end;
					end
				done;
			  done;
		  done; -1
	  end
	  with Wynik(x) -> x;;
	  
assert(przelewanka [|(1,1)|] = 1);;
