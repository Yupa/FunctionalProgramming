Rozpakuj archiwa i wpisz w konsoli:
ocamlbulid gen_out.native
sudo chmod +x test.sh
./test.sh

Spowoduje to puszczenie twojego rozwiązania na testach z katalogu in i porównanie ich z odpowiedziami
wygenerowanymi moim programem z katalogu out. Skrypt wypisze nazwy wszystkich testów których odpowiedzi różnią się z wzorcowymi (jak nie wypisze nic to znaczy, że wszystkie odpowiedzi się zgadzają).

Dodatkowo w archwum gen.zip są pliki gen_test.ml, gen_outs.sh i gen_tests.sh. Pierwszy z nich generuje test
w formacie: [|(x1, y1); ... (xn, yn)|] i wypisuje go jako ciąg x1\ny1\n..xn\nyn gdzie \n to znak
końca lini. Drugi z nich generuje outy za pomocą przelewanki to katalogu out. Trzeci z nich bierze 1 argument i generuje tyle testów za pomocą pierwszego w katalogu in.
Argumenty dla gen_test.ml zapisuje się w pliku args.txt w kolejnych liniach. Są to kolejno max kubków,
max wynik, max pojemność.

Testy są generowane poprzez symulowanie losowych operacji, więc każdy test można rozwiązać
(nie są też bardzo losowe...).

paczka small.zip zawiera 2000 małych testów (max 5 kubków o pojemności 10) u mnie liczy się ~30s.
