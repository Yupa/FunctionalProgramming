open Przelewanka;;

let wczytaj () =
  let n = read_int () in
  let tab = Array.make n (0, 0) in
  for i = 0 to n - 1 do
    let a = read_int () in
    let b = read_int () in
    tab.(i) <- (a, b);
  done;
  tab;;

print_int (przelewanka (wczytaj ()));;
