(* Rafal Pragacz, *)
(* review Wiktoria Kosny *)
(* testy z grupy na facebooku *)

exception Wynik of int;;

(* najwiekszy wspolny dzielnik *)
let rec gcd x y =
  if x = 0 then y
  else if y = 0 then x
  else if y > x then gcd x (y mod x)
  else if x > y then gcd (x mod y) y
  else x;;

let gcd_of_glasses t = Array.fold_left gcd t.(0) t;;

(* sprawdza czy jakas szklanka ma byc pelna lub pusta *)
let rec full_empty t s i =
  if i = -1 then true
  else if t.(i) = 0 || t.(i) = s.(i) then false
  else full_empty t s (i - 1);;

(* W szklankach mozemy osiagnac tylko wielokrotnosci *)
(* NWD pojemnosci tych szklanek, wiec sprawdzamy najpierw czy *)
(* wartosci oczekiwane sa przez niego podzielne *)
(* nastepnie sprawdzamy czy ktoras ze szklanek ma byc pelna lub pusta *)
(* bo inaczej nie ma rozwiazania, na koncu odpalamy backtrack *)

let przelewanka l =
  let n = Array.length l in
  if n = 0 then 0 else
  let my_hash = Hashtbl.create (n * n * 100) in
  let cap = Array.map fst l in
  let demand = Array.map snd l in
  let q = Queue.create () in
  let gcd = gcd_of_glasses cap in
  let gcd2 = gcd_of_glasses demand in
  if gcd = 0 || gcd2 = 0 then 0
  else if (not (gcd2 mod gcd = 0)) || (full_empty demand cap (n - 1)) then -1
  else
    try
      Queue.add ((Array.make n 0), 0) q;
      while not (Queue.is_empty q) do
        let (t, moves) = Queue.pop q in
        if t = demand then raise (Wynik moves);
        for i = 0 to (n - 1) do
          for j = 0 to (n - 1) do
            if i = j then
            let t1 = Array.copy t in
            let t2 = Array.copy t in
            begin
              t1.(i) <- cap.(i);
              if not (Hashtbl.mem my_hash t1) then
              begin
                Queue.add (t1, moves + 1) q;
                Hashtbl.add my_hash t1 (moves + 1);
              end;
              t2.(i) <- 0;
              if not (Hashtbl.mem my_hash t2) then
              begin
                Queue.add (t2, moves + 1) q;
                Hashtbl.add my_hash t2 (moves + 1);
              end;
            end
            else              
            let t3 = Array.copy t in
            begin
              t3.(j) <- min cap.(j) (t.(i) + t.(j));
              t3.(i) <- max (t.(i) + t.(j) - cap.(j)) 0;
              if not (Hashtbl.mem my_hash t3) then
              begin
                Queue.add (t3, moves + 1) q;
                Hashtbl.add my_hash t3 (moves + 1);
              end;
            end
          done;
        done;
      done; -1
    with Wynik(x) -> x;;
