open PMap;;

let genlist n range =
  let rec pom n map a =
    if n = 0 then a else
    let x = ref (Random.int range) in
    while mem !x map do x := Random.int range done;
    pom (n - 1) (add !x true map) (!x::a)
  in
  let rec pom2 n a =
    if n > 0 then pom2 (n - 1) ((Random.int (100000000), n - 1)::a) else a in
  let n = min n range in
  if range - n < n then
    List.filter (fun x -> x >= 0)
      (List.mapi (fun i (pr, v) -> if i < n then v else -1) (List.sort compare (pom2 range [])))
  else pom n empty [];;

let inittable n (mn, mx) =
  let map = ref empty in
   Array.init n (fun _ -> 
     let x = ref (mn +. Random.float (mx -. mn)) in
     while mem x !map do x:= mn +. Random.float (mx -. mn) done;
     map := add x true !map;
     !x);;
 (* Array.init n (fun _ ->
    let x = ref (mn + Random.int (mx - mn)) in
    while mem x !map do x := mn + Random.int (mx - mn) done;
    map := add x true !map;
    !x);; *)
 
let gendag maxn maxzero maxkraw maxdod range =
  let n = Random.int (maxn - 1) + 2 in
  let tab = Array.make n [] in
  let tmap = inittable n range in
  let rec pom ak =
    let il = Random.int (maxkraw) + 1 in
    List.iter (fun x -> tab.(x) <- ak::(tab.(x))) (genlist il ak);
    if ak < n - 1 then pom (ak + 1) in
  pom (min (Random.int maxzero + 1) (n - 1));
  let dod = Random.int (maxdod + 1) in
  List.iter2 (fun x y ->if not (List.mem y tab.(x)) then tab.(x) <- y::(tab.(x)))
    (genlist dod n) (genlist dod n);
  Array.to_list (Array.mapi (fun i l -> (tmap.(i), List.map (fun x -> tmap.(x)) l)) tab);;

