open PMap;;
open Topol;;
open Gen;;

let check g list =
  let graf = List.fold_left (fun m (x, l) -> add x l m) empty g in
  let mapa = empty in
  let rec pom list mapa =
    match list with
    | [] -> true
    | h::t ->
	let mapa = add h true mapa in
	let rec iter = function
	  | [] -> true
	  | h::t -> if mem h mapa then false else iter t in
	if mem h graf && iter (find h graf) then pom t mapa else false
  in
  pom list mapa;;

let findloop g =
  let graf = List.fold_left (fun m (x, l) -> add x l m) empty g in
  let mapa = ref empty in

  let rec dfs x =
    if mem x !mapa then true else
    begin
      mapa := add x true !mapa;
      let rec pom = function
	| [] -> true
	| h::t -> if mem x !mapa && find x !mapa then false else dfs x && pom t in
      let wyn = pom (find x graf) in
      mapa := add x false !mapa;
      wyn
    end in
  
  let rec pom = function
    | [] -> false
    | (x, l)::t -> if dfs x then pom t else true in
  pom g;;


let wypisz l =
  (*List.iter (fun (x, l) ->
    print_string ((string_of_int x)^": ");
    List.iter (fun x -> print_string ((string_of_int x)^" ")) l;
    print_newline ()) l;;*)
  List.iter (fun (x, l) ->
    print_string ((string_of_float x)^": ");
    List.iter (fun x -> print_string ((string_of_float x)^" ")) l;
    print_newline ()) l;;


let rec testoj maxn maxzero maxkraw maxdod range licznik =
  let l = gendag maxn maxzero maxkraw maxdod range in
  let t = Sys.time () in
  let wyn = try
    let w = topol l in
    check l w
  with Cykliczne ->
    print_string "cykl - ";
    findloop l in
  let t = Sys.time () -. t in
  if wyn then
    begin
      print_endline ((string_of_int licznik)^": "^(string_of_float t)^"s");
      testoj maxn maxzero maxkraw maxdod range (licznik + 1)
    end
  else wypisz l;;


(* print_endline "Podaj maksymalną liczbę wieżchołków w grafie";; *)
let maxn = 1000
(* print_endline "Podaj maksymalną liczbę wieżchołków do których nie wchodzą żadne krawędzie (minimum 1)";; *)
let maxzero = 50
(* print_endline "Podaj maksymalną liczbę krawędzi wychodzących z każdego wieżchołka";; *)
let maxkraw = 100
(* print_endline "Podaj maksymalną liczbę dodatkowych krawędzi w grafie (mogą powodować cykle)";; *)
let maxdod = 3
(* print_endline "Podaj zakres liczb na wejściu (2 liczby w osobnych liniach odpowiedniego typu)";; *)
 let p = -1000000.0
 let k = 1000000.0;;

(*let p = read_int ();;
let k = read_int ();; *)

Random.self_init ();
testoj maxn maxzero maxkraw maxdod (p, k) 0;;
