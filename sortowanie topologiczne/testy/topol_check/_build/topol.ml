(* Rafal Pragacz, *)
(* review Jan Tabaszewski *)

open PMap;;

(*SORTOWANIE PRZEZ OBGRYZANIE*)

exception Cykliczne;;

(* funkcja do tworzenia mapy ze stopniami wierzcholkow *)
let h acc x =
  if mem x acc then add x (find x acc + 1) acc
  else add x 1 acc;;

(* funkcja do tworzenia mapy z krawedziami oraz mapy ze stopniami wierzcholkow *)
let g (m1, m2) el =
  let stopnie = List.fold_left h m2 (snd el) in
  if mem (fst el) stopnie then
    (add (fst el) (snd el) m1, stopnie)
  else
    (add (fst el) (snd el) m1, add (fst el) 0 stopnie);;

(* funkcja ktora dorzuca do listy element jesli nie wchodzi do niego zadna krawedz *)
(* z wierzcholkow ktore nie sa jeszcze na liscie wynikowej *)
let aux (lista, stopnie) x =
  let st = find x stopnie in
  if st = 1 then (x :: lista, stopnie)
  else (lista, add x (st - 1) stopnie);;

(* edges - mapa krawedzi, deg / degg - mapa z iloscia krawedzi wchodzacych *)
(* zera - lista wierzcholkow o stopniu wchodzacym zero *)
(* dlugosc - liczba elementow ktore powinny zostac wypisane *)
let topol l =
  let (edges, deg) = List.fold_left g (empty, empty) l in
  let f a x = if find (fst x) deg = 0 then (fst x) :: a else a in
  let zera = List.fold_left f [] l in
  let dlugosc = fold (fun _ a -> a + 1) deg 0 in
  let rec sortuj dlug wynik (l, degg) =
    match l with
    | [] -> if dlug <> 0 then raise Cykliczne else wynik
    | hd :: tl -> if mem hd edges
      then sortuj (dlug - 1) (hd :: wynik) (List.fold_left aux (tl, degg) (find hd edges))
      else sortuj (dlug - 1) (hd :: wynik) (tl, degg)
  in List.rev (sortuj dlugosc [] (zera, deg));;
