Skrypt testujący do zadania topol.
Aby użyć wystarczy uruchomić polecenie "ocamlbuild test.native" (bez cudzysłowów) w katalogu z
rozpakowanym archiwum.

Skrypt generuje daga sortuje go topologicznie, sprawdza poprawność sortowania, lub istnienie cyklu
w przypadku podniesienia wyjątku "Cykliczne".
W przypadku poprawnego uruchomienia wypisywany jest numer testu i czas działania programu
(sortowania i weryfikacji która jest przeprowadzana w czasie O(n * log n) przy użyciu modułu PMap).
Jeśli został podniesiony wyjątek "Cykliczne" wypisywane jest również "cykl".

Na wejściu podaje się między innymi liczbę dodatkowych krawędzi
(może być 0 - nie będzie na pewno cyklu). Liczba ta określa ile losowych krawędzi zostanie dodanych
do dagu. Im większa, tym większa szansa na utworzenie się cyklu.

Skrypt domyślnie testuje na floatach (wartości na liście są floatami). Aby to zmienić trzeba
odkomentować ( i zakomentować ) odpowiednie miejsca w plikach:
test.ml
gen.ml
gen.mli

