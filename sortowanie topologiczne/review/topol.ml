(* author: Jan Tabaszewski *)
(* reviewer: Rafał Pragacz *)

open PMap;;

exception Cykliczne;;

let topol l =
  let lists =(* map for quick access to adjacency list *)
    let f acc (v, vl) = add v vl acc in
    List.fold_left f empty l
  in
  (* DFS from v; adds reachable vertexes to acc in post order *)
  let rec dfs (vis, acc) (v, vl) =
    if mem v vis then (vis, acc) else
      let f (visf, accf) u =
        if not (mem u lists) then
          if not (mem u visf) then (add u 1 visf, u::accf)
          else (visf, accf)
        else dfs (visf, accf) (u, find u lists)
      in
      let (new_vis, new_acc) = List.fold_left f (add v 1 vis, acc) vl in
      (new_vis, v::new_acc)
  in
  (* creating topological order of graph l *)
  let (vis, res) = List.fold_left dfs (empty, []) l in
  
  let to_right (acc, boo) v =
    let f boo_in u = if not (mem u acc) || u = v then true else boo_in in
    let new_boo = if not (mem v lists) then boo
      else List.fold_left f boo (find v lists)
    in
    (remove v acc, new_boo)
  in
  (* have to check whether all edges go to the right of the list "res" *)
  (* if not, then l was not a DAG *)
  if snd (List.fold_left to_right (vis, false) res)
    then raise Cykliczne else res;;
