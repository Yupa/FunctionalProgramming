open Leftist

let pusta = empty;;
let a = add 1 pusta;;
let b = join a pusta;;
let c = add 42 pusta;;
let d = add 41 c;;
let e = add 42 d;;
let f = add 40 e;;
let (g,h) = delete_min f;;
let (i,j) = delete_min h;;
let (k,l) = delete_min j;;
let (m,n) = delete_min l;;

assert(is_empty pusta);
assert(not (is_empty a));
assert(g = 40);
assert(i = 41);
assert(k = 42);
assert(m = 42);
assert(not (is_empty h));
assert(not (is_empty j));
assert(not (is_empty l));
assert(is_empty n);;