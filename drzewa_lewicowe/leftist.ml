(*Autor - Rafal Pragacz, review - Lukasz Kondraciuk,
testy - http://pastebin.com/3xvNKn9b*)

(*typ = 
wezel (lewy syn, priorytet, prawy syn, dlugosc prawej sciezki) | null*)
type 'a queue = Node of ('a queue)*('a)*('a queue)*int | Null

(*zwraca dlugosc prawej sciezki z danego wierzcholka*)
let size q =
  match q with
  | Null -> -1
  | Node(_, _, _, s) -> s

(*Zmienia drzewo, aby bylo lewicowe*)
let make_leftist q =
  match q with
  | Null -> Null
  | Node(l, w, r, _) ->
    if size l >= size r then Node(l, w, r, size r + 1)
    else Node(r, w, l, size l + 1)

exception Empty

let empty = Null

let is_empty q = (q = Null)

let rec join q1 q2 =
  match (q1, q2) with
  | (Null, _) -> q2
  | (_, Null) -> q1
  | (Node(l1, w1, r1, s1), Node(l2, w2, r2, s2) ) ->
    if w1 < w2 then
      make_leftist (Node(l1, w1, (join r1 q2), 42))
    else
      make_leftist (Node(l2, w2, (join r2 q1), 42))

let rec add x q =
  join (Node(Null, x, Null, 0)) q

let delete_min q =
  match q with
  | Null -> raise Empty
  | Node(l, w, r, _) -> (w, (join l r) )